# Use to allow GCP default service accounts to authenticate with Vault and be assigned
# a specific set of credentials.
module "gcp_auth" {
  source = "./modules/gcp-auth"

  GCPPROJECT_VAULTPOLICY_MAP = var.GCPPROJECT_VAULTPOLICY_MAP
}

# Use to assign an approle name with a given set of policies.
module "approle_auth" {
  source = "./modules/approle-auth"

  APPROLE_VAULTPOLICY_MAP = var.APPROLE_VAULTPOLICY_MAP
}

# Use this to setup OIDC authentication.
module "oidc_auth" {
  source = "./modules/oidc-auth"

  VAULT_MANAGED_ADDRESS = var.VAULT_MANAGED_ADDRESS
  OIDC_CLIENT_ID = var.OIDC_CLIENT_ID
  OIDC_CLIENT_SECRET = var.OIDC_CLIENT_SECRET
  OIDC_VAULTPOLICY_MAP = var.OIDC_VAULTPOLICY_MAP
  OIDC_DEFAULT_ROLE = var.OIDC_DEFAULT_ROLE
}