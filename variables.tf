variable "GCPPROJECT_VAULTPOLICY_MAP" {
  description = "Map of GCP project id to a list of vault policies, i.e. { prd_projectname_12345: [ \"policy1\", \"policy2\"] }.  The module will assign the policies to the default service account for the given project.  To use this, a service account on GCP can use the Vault CLI and issue the following command `vault login -method=gcp role=\"project-id\" service_account=\"authenticating-account@project-id.iam.gserviceaccount.com\" project=\"project-id\"`"
  type = map
  default = {}
}

variable "APPROLE_VAULTPOLICY_MAP" {
  description = "Map of AppRole name to a list of vault policies, i.e. { gitlab-runner1: [ \"policy1\", \"policy2\"] }.  The module will assign the policies to the given approle name.  The result of this will be a new path will be mounted in Vault that you can then generate an AppRole RoleId and AppRole SecretId with.  These will be accessible with `vault read auth/approle/role/gitlab-runner1/role-id` and `vault write -f auth/approle/role/gitlab-runner1/secret-id`"
  type = map
  default = {}
}

variable "OIDC_CLIENT_ID" {
  description = "This is your OIDC Client ID when setting up the OIDC app in the GCP console"
  default = ""
}

variable "OIDC_CLIENT_SECRET" {
  description = "This is your OIDC Client Secret when setting up the OIDC app in the GCP console"
  default = ""
}

variable "VAULT_MANAGED_ADDRESS" {
  description = "This is managed domain hosting your Vault instance.  This must be a verified domain that you have mapped to Vault - i.e. https://vault.myorg.com"
  default = ""
}

variable "OIDC_VAULTPOLICY_MAP" {
  description = "Map of OIDC requested roles to a list of vault policies, i.e. { team: [ \"policy1\", \"policy2\"] }"
  type = map
  default = {}
}

variable "OIDC_DEFAULT_ROLE" {
  description = "This is the default role that will be used if none is explictly requested for when loggin in.  The default is fine to use until you get more advanced"
  default = "default"
}
