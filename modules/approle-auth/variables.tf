variable "APPROLE_VAULTPOLICY_MAP" {
  description = "Map of AppRole name to a list of vault policies, i.e. { gitlab-runner1: [ \"policy1\", \"policy2\"] }.  The module will assign the policies to the given approle name.  The result of this will be a new path will be mounted in Vault that you can then generate an AppRole RoleId and AppRole SecretId with.  These will be accessible with `vault read auth/approle/role/gitlab-runner1/role-id` and `vault write -f auth/approle/role/gitlab-runner1/secret-id`"
  type = map
  default = {}
}