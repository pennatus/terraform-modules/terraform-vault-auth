# Create the auth backend for GCP resources to authenticate with (i.e. VMs and Cloud Run)
resource "vault_auth_backend" "approle_auth_backend" {
  type = "approle"
  path = "approle"
}

resource "vault_approle_auth_backend_role" "approle_role" {
  for_each        = var.APPROLE_VAULTPOLICY_MAP

  backend        = vault_auth_backend.approle_auth_backend.path
  role_name      = each.key
  token_policies = each.value
}