resource "vault_jwt_auth_backend" "oidc_auth_backend" {
  count = length(var.OIDC_CLIENT_ID) == 0 ? 0: length(var.OIDC_CLIENT_SECRET) == 0 ? 0 : 1
  description  = "GCP OIDC Backend"
  path = "oidc"
  type = "oidc"
  oidc_discovery_url = "https://accounts.google.com"
  oidc_client_id = var.OIDC_CLIENT_ID
  oidc_client_secret = var.OIDC_CLIENT_SECRET
  default_role = var.OIDC_DEFAULT_ROLE

  lifecycle {
    ignore_changes = [
      # Ignore changes to oidc_client_secret since this is a sensitive variable and appears
      # to change every apply since the state file doesn't store the actual value.
      oidc_client_secret
    ]
  }
}

resource "vault_jwt_auth_backend_role" "oidc_auth_role" {
  for_each              = length(var.OIDC_CLIENT_ID) == 0 ? {}: length(var.OIDC_CLIENT_SECRET) == 0 ? {} : var.OIDC_VAULTPOLICY_MAP

  backend               = vault_jwt_auth_backend.oidc_auth_backend[0].path
  role_name             = each.key
  token_policies        = concat(["default"], each.value)
  user_claim            = "sub"
  role_type             = "oidc"
  allowed_redirect_uris = ["${var.VAULT_MANAGED_ADDRESS}/ui/vault/auth/oidc/oidc/callback", "http://localhost:8250/oidc/callback"]
  bound_audiences       = [var.OIDC_CLIENT_ID]
}