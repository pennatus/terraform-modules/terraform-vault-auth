variable "OIDC_CLIENT_ID" {
  description = "This is your OIDC Client ID when setting up the OIDC app in the GCP console"
  default = ""
}

variable "OIDC_CLIENT_SECRET" {
  description = "This is your OIDC Client Secret when setting up the OIDC app in the GCP console"
  default = ""
}

variable "VAULT_MANAGED_ADDRESS" {
  description = "This is managed domain hosting your Vault instance.  This must be a verified domain that you have mapped to Vault - i.e. https://vault.myorg.com"
  default = ""
}

variable "OIDC_VAULTPOLICY_MAP" {
  description = "Map of OIDC requested roles to a list of vault policies, i.e. { team: [ \"policy1\", \"policy2\"] }"
  type = map
  default = {}
}

variable "OIDC_DEFAULT_ROLE" {
  description = "This is the default role that will be used if none is explictly requested for when loggin in.  The default is fine to use until you get more advanced"
  default = "team"
}