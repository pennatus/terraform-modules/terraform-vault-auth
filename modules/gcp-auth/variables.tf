variable "GCPPROJECT_VAULTPOLICY_MAP" {
  description = "Map of GCP projects to a list of vault policies, i.e. { prd_projectname = { project_id = \"prd_projectname_12345\", token_policies = [ \"policy1\", \"policy2\"] } }.  The module will assign the policies to the default service account for the given project.  To use this, a service account on GCP can use the Vault CLI and issue the following command `vault login -method=gcp role=\"project-id\" service_account=\"authenticating-account@project-id.iam.gserviceaccount.com\" project=\"project-id\"`"
  type = map
  default = {}
}