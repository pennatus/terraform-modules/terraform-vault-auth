# Create the auth backend for GCP resources to authenticate with (i.e. VMs and Cloud Run)
resource "vault_auth_backend" "gcp_auth_backend" {
  type = "gcp"
  path = "gcp"
}

data "google_project" "gcp_project" {
  for_each   = var.GCPPROJECT_VAULTPOLICY_MAP

  project_id = each.value.project_id
}


resource "vault_gcp_auth_backend_role" "gcp_role" {
  for_each               = var.GCPPROJECT_VAULTPOLICY_MAP

  role                   = each.value.project_id
  type                   = "iam"
  backend                = vault_auth_backend.gcp_auth_backend.path
  bound_projects         = [ each.value.project_id ]
  bound_service_accounts = [ "${data.google_project.gcp_project[each.key].number}-compute@developer.gserviceaccount.com", "${data.google_project.gcp_project[each.key].id}@appspot.gserviceaccount.com" ]
  token_policies         = each.value.token_policies
}