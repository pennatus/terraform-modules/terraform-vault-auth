## Purpose

The `terraform-vault-auth` module is used to create a set of authorizations for an Vault Auth type, such as GCP, AppRole, OIDC) based on a set of pre-defined Vault policies like 'prd-product-owner'.  This allows an entity (Vault authenticated user) to read from the paths (Vault roles) that it has access to.

An `entity` is a type of user which authenticates with AppRole, GCP Service Account, GSuite Group, Gitlab Group, etc...  Entities could be humans, gitlab.com, terraform.io, et.  The types of entities supported are documented in the [Inputs](#inputs) section below.

For example, a "team" auth role name can be mapped to a Shared product using module.shared_product.vault_viewer_policies to grant read access to all shared resources like docker registries.

When using this code, whenever possible try to reference ids and policy names from other managed resources instead of hardcoding things like project ids and policy names.

## Usage

### AppRole

Once you have added a new entry in the policy map for AppRole you will need to generate a RoleId and SecretId for the user.

    vault read auth/approle/role/my-role/role-id
    vault write -f auth/approle/role/my-role/secret-id

Role ID is public and can be built into images, but secret-id should be injected after the resources is deployed.

## Example

```hcl
module "vault_auth" {
  source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-vault-auth"

  APPROLE_POLICY_MAP = {
    "terraform-is-org": [ "vault-admin" ]
  }

  GCP_PROJECT_POLICY_MAP = {
    "${module.cool_product.env_project_map.prd.id}": [
      module.cool_product.vault_owner_policies.prd.name,
      module.shared_db.vault_owner_policies.cool_db.name
    ]
  }
}
```

In this particular example we are mapping a specific APPROLE name `terraform-is-org` to a special policy named `vault-admin`.  This means when we generate a `role-id` and `secret-id` for this role we will be granted all the capabilities of the `vault-admin` policy.  This particular mapping is useful for the `is-org` project that requires admin level access to resources since it is managing products and databases.

We are also mapping the default GCP service account created in the `prd` environment for `cool_product` to 2 vault policies: owner policy of the `cool_product` in the `env` environment, amd owner of the `cool_db`.  When compute resources in the `prd` - `cool_product` authenticate with GCP they will be granted full access to resources within their GCP project and they will also have full access to the `cool_db`.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| APPROLE\_VAULTPOLICY\_MAP | Map of AppRole name to a list of vault policies, i.e. \{ gitlab-runner1: \[ "policy1", "policy2"\] \}.  The module will assign the policies to the given approle name.  The result of this will be a new path will be mounted in Vault that you can then generate an AppRole RoleId and AppRole SecretId with.  These will be accessible with `vault read auth/approle/role/gitlab-runner1/role-id` and `vault write -f auth/approle/role/gitlab-runner1/secret-id` | map | `{}` | no |
| GCPPROJECT\_VAULTPOLICY\_MAP | Map of GCP project id to a list of vault policies, i.e. \{ prd\_projectname\_12345: \[ "policy1", "policy2"\] \}.  The module will assign the policies to the default service account for the given project.  To use this, a service account on GCP can use the Vault CLI and issue the following command `vault login -method=gcp role="project-id" service\_account="authenticating-account@project-id.iam.gserviceaccount.com" project="project-id"` | map | `{}` | no |
| OIDC\_CLIENT\_ID | This is your OIDC Client ID when setting up the OIDC app in the GCP console | string | `""` | no |
| OIDC\_CLIENT\_SECRET | This is your OIDC Client Secret when setting up the OIDC app in the GCP console | string | `""` | no |
| OIDC\_DEFAULT\_ROLE | This is the default role that will be used if none is explictly requested for when loggin in.  The default is fine to use until you get more advanced | string | `"default"` | no |
| OIDC\_VAULTPOLICY\_MAP | Map of OIDC requested roles to a list of vault policies, i.e. \{ team: \[ "policy1", "policy2"\] \} | map | `{}` | no |
| VAULT\_MANAGED\_ADDRESS | This is managed domain hosting your Vault instance.  This must be a verified domain that you have mapped to Vault - i.e. https://vault.myorg.com | string | `""` | no |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
